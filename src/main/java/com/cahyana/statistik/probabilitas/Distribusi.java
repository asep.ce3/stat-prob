package com.cahyana.statistik.probabilitas;

public class Distribusi {
    
    public static double pbkMaximal(double probabilitasSukses, int percobaanSukses, int bayakPercobaan) {
        double hasil = 0;
        for (int i=0; i<=percobaanSukses; i++) {
            hasil += binomial(probabilitasSukses, i, bayakPercobaan);
        }
        
        return hasil;
    }
    
    public static double pbkMinimal(double probabilitasSukses, int percobaanSukses, int bayakPercobaan) {
        double hasil = 0;
        for (int i=bayakPercobaan; i>=percobaanSukses; i--) {
            hasil += binomial(probabilitasSukses, i, bayakPercobaan);
        }
        
        return hasil;
    }
    
    public static double binomial(double probabilitasSukses, int percobaanSukses, int bayakPercobaan) {
        
        double kom = kombinasi(percobaanSukses, bayakPercobaan);
        double px = Math.pow(probabilitasSukses, percobaanSukses);
        double pnx = Math.pow(1-probabilitasSukses, (bayakPercobaan-percobaanSukses));
        
        return kom * px * pnx;
    }
    
    public static double poisson(double probabilitasSukses, int percobaanSukses, int banyakPercobaan) {
        double rataKejadian = mean(probabilitasSukses, banyakPercobaan);
        double pembilang = Math.pow(rataKejadian, percobaanSukses) * Math.pow(Math.E, -(rataKejadian));
        double penyebut = faktorial(percobaanSukses);
        return pembilang/penyebut;
    }
    
    public static double ppkMaximal(double probabilitasSukses, int percobaanSukses, int banyakPercobaan) {
        double hasil = 0;
        for (int i=0; i<=percobaanSukses; i++) {
            hasil += poisson(probabilitasSukses, i, banyakPercobaan);
        }
        return hasil;
    }
    
    public static double hipergeometrik(int semua, int rusak, int diambil, int diambilFilter) {
        int N = semua;
        int N1 = rusak;
        int N2 = N - N1;
        int n = diambil;
        int k = diambilFilter;

        double a = kombinasi(k, N1);
        double b = kombinasi(n-k, N2);
        double c = kombinasi(n, N);
        
        return (a*b)/c;
    }
    
    public static double phkMaximal(int semua, int rusak, int diambil, int diambilFilter) {
        double hasil =0;
        for (int i=0; i<=diambilFilter; i++) {
            hasil += hipergeometrik(semua, rusak, diambil, i);
        }
        return hasil;
    } 
    
    public static double mean(double probabilitasSukses, int banyakPercobaan) {
        return banyakPercobaan*probabilitasSukses;
    }
    
    public static double kombinasi(int bawah, int atas) {
        int bawah2 = atas - bawah;
        int batas = Math.max(bawah, bawah2);
        double pembilang = faktorial(atas, batas);
        
        double penyebut;
        if (batas == bawah) {
            penyebut = faktorial(bawah2);
        } else {
            penyebut = faktorial(bawah);
        }
                
        return pembilang/penyebut;
    }
    
    public static int faktorial(int num, int batas) {
        if (num == 0) return 1;
        if (num > 0 && num <=2) return num;
        
        int hasil = 1;
        for (int i=num; i>batas; i--) {
            hasil *=i;
        }
        
        return hasil;
    }
    
    public static int faktorial(int num) {
        if (num == 0) return 1;
        if (num > 0 && num <=2) return num;
        
        int hasil = 1;
        for (int i=1; i<=num; i++) {
            hasil *= i;
        }
        
        return hasil;
    }
}
