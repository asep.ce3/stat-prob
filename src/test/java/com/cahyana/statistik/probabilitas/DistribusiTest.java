
package com.cahyana.statistik.probabilitas;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

/**
 *
 * @author asepc
 */
public class DistribusiTest {
    
    public DistribusiTest() {
    }
    
    @Test
    void factorialTest() {
        int hasil = Distribusi.faktorial(5);
        assertEquals(120, hasil);
    }
    
    @Test
    void kombinasiTest() {
        double hasil = Distribusi.kombinasi(2, 5);
        assertEquals(10.0, hasil);
    }
    
    @Test
    void meanTest() {
        double hasil = Distribusi.mean(2.0/5.0, 12);
        assertEquals(4.8, hasil, 0.01);
    }
    
    @Test
    void binomialTest() {
        double hasil = Distribusi.binomial(0.6, 0, 5);
        assertEquals(0.01024, hasil, 0.001);
    }
    
    @Test
    void pbkMaksimalTest() {
        double hasil = Distribusi.pbkMaximal(0.6, 2, 5);
        assertEquals(0.31744, hasil, 0.0001);
    }
    
    @Test
    void pbkMinimalTest() {
        double hasil = Distribusi.pbkMinimal(0.6, 4, 5);
        assertEquals(0.33696, hasil, 0.0001);
    }
    
    @Test
    void poissonTest() {
        double hasil = Distribusi.poisson(20.0/1000.0, 3, 150);
        assertEquals(0.224, hasil, 0.0001);
    }
    
    @Test
    void ppkMaximalTest() {
        double hasil = Distribusi.ppkMaximal(100.0/10_000.0, 2, 1000);
        assertEquals(0.002769, hasil, 0.000001);
    }
    
    @Test
    void hipergeometrik() {
        double hasil = Distribusi.hipergeometrik(42, 7, 5, 2);
        assertEquals(0.16157, hasil, 0.0001);
    }
    
    @Test
    void phkMaximalTest() {
        double hasil = Distribusi.phkMaximal(50, 5, 4, 4);
        assertEquals(0.99994, hasil, 0.0001);
    }
}
